//
//  BaseVC.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Helper Methods
    
    func showMessage(text: String) {
        let alert = UIAlertController(title: "iMusic",
                                      message: text,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "O.K.",
                                     style: .default,
                                     handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - FailureHandlerDelegate

extension BaseVC: FailureHandlerDelegate {
    func handleError(_ error: Error) {        
        guard let error = error as? MusicError else { return }
        
        var message = ""
        
        switch error {
        case .noInternetConnection:
            message = "No Internet Connection"
            
        case .invalidResponse:
            message = "Invalid response"
            
        case .unknownError(let msg):
            message = msg
        }
        
        DispatchQueue.main.async {
            self.showMessage(text: message)
        }
    }
}
