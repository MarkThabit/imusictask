//
//  MainVC.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

class MainVC: BaseVC {
    
    // MARK: - iVars
    
    private var searchView: UISearchBar!
    fileprivate var songList: [MusicItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - Helper Methods
    
    private func setupView() {
        // Show navBar
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1960576177, green: 0.1960916817, blue: 0.1960501969, alpha: 1)
        
        // Configure search bar
        searchView = UISearchBar(frame: CGRect.zero)
        searchView.delegate = self
        searchView.becomeFirstResponder()
        searchView.backgroundColor = .clear
        searchView.placeholder = "Search Songs"
        searchView.returnKeyType = .done
        
        // Add search bar to navBar and hide back button
        navigationItem.titleView = searchView
        navigationItem.hidesBackButton = true
        
        // Setup tableView
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: SongCell.reusableIdentifier,
                                 bundle: nil),
                           forCellReuseIdentifier: SongCell.reusableIdentifier)
    }
    
    fileprivate func startSearch() {
        guard let query = searchView.text, query.count > 3 else { return }
        
        HTTPRequest.shared.request(request: ServerAPI.searchSongs(query: query),
                                   modelType: [MusicItem].self,
                                   success: { result in
                                    guard let musicList = result as? [MusicItem] else { return }
                                    
                                    // This will reload the data as has an observer for changes
                                    self.songList = musicList
        },
                                   failure: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let song = sender as? MusicItem,
            let musicItemDetailsVC = segue.destination as? MusicItemDetailsVC,
            segue.identifier == "MusicItemDetailsVC" else { return }
        
        musicItemDetailsVC.song = song
    }
}

// MARK: - UISearchBarDelegate

extension MainVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        startSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Dismiss the keyboard in case user tap done button
        searchBar.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource

extension MainVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SongCell.reusableIdentifier,
                                                       for: indexPath) as? SongCell else {
                                                        return UITableViewCell()
        }
        
        // This statment will resposible to configure the cell
        cell.song = songList[indexPath.row]
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension MainVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MusicItemDetailsVC", sender: songList[indexPath.row])
    }
}

// MARK: - UIScrollViewDelegate

extension MainVC: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchView.resignFirstResponder()
    }
}
