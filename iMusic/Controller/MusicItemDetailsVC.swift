//
//  MusicItemDetailsVC.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

class MusicItemDetailsVC: BaseVC {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var songImageView: RoundedImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var artistNameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var genreStackView: UIStackView!
    
    // MARK: - iVars
    
    var song: MusicItem!

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - Helper Methods
    
    private func setupView() {
        // Add search bar to navBar and hide back button
        navigationItem.hidesBackButton = false
        
        // Setup navBar
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        // Set the view title
        navigationItem.title = song.title
        
        if let defaultImageSize = self.song.cover?.default {
            songImageView.loadImage(urlString: defaultImageSize)
        }
        
        titleLbl.text = song.title
        artistNameLbl.text = song.artist?.name
        typeLbl.text = song.type
        durationLbl.text = createDurationText()
        dateLbl.text = song.publishingDate?.toString
        
        song.genreList?.forEach {
            let label = UILabel()
            label.textColor = .white
            label.font = titleLbl.font
            label.text = $0
            
            genreStackView.addArrangedSubview(label)
        }
    }
    
    private func createDurationText() -> String {
        guard let duration = song.duration else { return "" }
        
        let minutes = duration / 60
        let seconds = duration % 60
        
        return "\(minutes):\(seconds)"
    }
}
