//
//  SplashVC.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

class SplashVC: BaseVC {
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestAccessToken()
    }
    
    // MARK: - Helper Methods
    
    private func requestAccessToken() {
        HTTPRequest.shared.request(request: ServerAPI.getAccessToken,
                                   modelType: AccessTokenModel.self,
                                   success: { [weak self] accessTokenModel in
                                    guard let `self` = self,
                                        let accessTokenModel = accessTokenModel as? AccessTokenModel else { return }
                                    
                                    self.saveAccessToken(accessTokenModel)
                                    self.performSegue(withIdentifier: "MainVC", sender: nil)
            },
                                   failure: self)
    }
    
    private func saveAccessToken(_ accessTokenModel: AccessTokenModel) {
        guard let accessToken = accessTokenModel.accessToken else { return }
        UserDefaults.standard.set(accessToken, forKey: Constants.UserDefaultsKeys.kAccessToken)
        UserDefaults.standard.synchronize()
    }
}
