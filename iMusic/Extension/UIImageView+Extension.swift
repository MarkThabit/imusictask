//
//  UIImageView+Extension.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

fileprivate let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImage(urlString: String) {
        // Remove current Image
        image = nil
        
        // Check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            image = cachedImage
            return
        }
        
        // otherwise download the image
        DispatchQueue.global(qos: .background).async {
            guard let url = URL(string: urlString.replacingOccurrences(of: "//", with: "http:/")),
                let data = try? Data(contentsOf: url) else {
                    return
            }
            
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    self.image = downloadedImage
                }
            }
        }
    }
}
