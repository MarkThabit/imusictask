//
//  AccessTokenModel.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

class AccessTokenModel: Decodable {
    
    // MARK: - iVars
    
    var accessToken: String?
    var tokenType: String?
    var expiresIn: String?
    
    // MARK: - Keys
    
    private enum CodingKeys: String, CodingKey {
        case accessToken
        case tokenType
        case expiresIn
    }
}
