//
//  Artist.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

class Artist: Decodable {
    
    // MARK: - iVars
    
    var id: Int?
    var name: String?
    
    // MARK: - Keys
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}
