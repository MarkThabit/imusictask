//
//  CoverImage.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

class CoverImage: Decodable {
    
    // MARK: - iVars
    
    var `default`: String?
    var large: String?
    var medium: String?
    var small: String?
    var template: String?
    var tiny: String?
    
    // MARK: - Keys
    
    private enum CodingKeys: String, CodingKey {
        case `default`
        case large
        case medium
        case small
        case template
        case tiny
    }
}
