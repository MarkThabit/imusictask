//
//  MusicItem.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

class MusicItem: Decodable {
    
    // MARK: - iVars
    
    var id: Int?
    var title: String?
    var artist: Artist?
    var cover: CoverImage?
    var type: String?
    var duration: Int?
    var genreList: [String]?
    var publishingDate: Date?
    
    // MARK: - Keys
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case artist = "mainArtist"
        case cover
        case type
        case duration
        case genreList = "genres"
        case publishingDate
    }
}
