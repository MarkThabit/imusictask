//
//  Connectivity.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation
import Network

class Connectivity {
    
    // MARK: - Singleton
    
    static let shared = Connectivity()
    
    // MARK: - iVars
    
    private let monitor = NWPathMonitor()
    var isConnected = false
    
    // MARK: - Initializer
    
    private init () { }
    
    // MARK: - Public
    
    func startListeningToConnectivityStatus() {
        setupMonitor()
        startMonitoring()
    }
    
    // MARK: - Helper Methods
    
    private func setupMonitor() {
         // Assign a closure to that monitor that will be triggered whenever network accessibility changes.
         // This needs to accept one parameter, which is an NWPath describing the network access that is currently possible.
        monitor.pathUpdateHandler = { [weak self] path in
            guard let `self` = self else { return }
            
            /*
               `status` describes whether the connection is currently available or not.
               `isExpensive` is set to true when using cellular data or
                when using WiFi that is hotspot routed through an iPhone’s cellular connection.
             */
            self.isConnected = (path.status == .satisfied)
            Logger.log(self.isConnected ? "We're connected!" : "No connection.")
            Logger.log("isExpensive: \(path.isExpensive)")
        }
    }
    
    private func startMonitoring() {
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
}
