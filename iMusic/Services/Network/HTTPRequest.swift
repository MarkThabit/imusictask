//
//  HTTPRequest.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

typealias successCallback = (Decodable) -> Void

class HTTPRequest {
    
    // MARK: - Singleton
    
    static let shared = HTTPRequest()
    
    // MARK: - Initializer
    
    private init () { }
    
    // MARK: - iVars
    
    var currentRequest: URLSessionDataTask!
    private let base = Bundle.main.object(forInfoDictionaryKey: Constants.AppURLKeys.kBaseURL) as? String ?? ""
    
    // MARK: - Class Functions
    
    func request<T: Decodable>(request: Request,
                               modelType: T.Type,
                               success successCallback: successCallback?,
                               failure failureHandler: FailureHandlerDelegate?) {
        // Check connectivity
        guard Connectivity.shared.isConnected else {
            failureHandler?.handleError(MusicError.noInternetConnection)
            return
        }
        
        guard let url = URL(string: base + request.path) else { return }
        
        // Setup URLRequest
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = request.headers
        
        // Check the HTTPMethod
        if let parameters = request.parameters {
            if request.method == .post { // Then we need to put the parameters in request body
                let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                urlRequest.httpBody = data
            } else if request.method == .get, // Then we need the parameters to be appended to url
                var urlComponents = URLComponents(string: base + request.path) {
                urlComponents.query = query(parameters)
                urlRequest.url = urlComponents.url
            }
        }
        
        currentRequest = URLSession.shared.dataTask(with: urlRequest,
                                                    completionHandler: { [weak self] data, urlResponse, error in
                                                        guard let `self` = self else { return }
                                                        
                                                        // Check if there any errors
                                                        guard error == nil else {
                                                            Logger.log(error!)
                                                            failureHandler?.handleError(MusicError.unknownError(messgae: error!.localizedDescription))
                                                            return
                                                        }
                                                        
                                                        // Try to parse the resonse
                                                        if let data = data,
                                                            let response = urlResponse as? HTTPURLResponse,
                                                            response.statusCode == 200 {
                                                            self.logResponse(data)
                                                            
                                                            // Setup the decoder
                                                            let jsonDecoder = JSONDecoder()
                                                            jsonDecoder.dateDecodingStrategy = .iso8601
                                                            let object: Decodable
                                                            
                                                            do { // Parse data
                                                                object = try jsonDecoder.decode(modelType,
                                                                                                from: data)
                                                            } catch {
                                                                failureHandler?.handleError(MusicError.invalidResponse)
                                                                return
                                                            }
                                                            
                                                            // Dispatch the success block to be excuted in main thread
                                                            DispatchQueue.main.async {
                                                                successCallback?(object)
                                                            }
                                                        }
        })
        
        currentRequest.resume()
    }
    
    /// Takes the parameter that would be send using get request and create the query for the request
    ///
    /// - Parameter parameters: Parameters need to be send as part of the url
    /// - Returns: Query needed to be appended to the url
    private func query(_ parameters: [String: Any]) -> String {
        let params = parameters.map { "\($0.key)=\($0.value)" }
        return params.joined(separator: "&")
    }
    
    func logResponse(_ data: Data) {
        #if DEBUG
        guard let request = currentRequest.originalRequest else { return }
        let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
        print("\n\n++ Response Log is request url: \(String(describing: request.url))\n\n")
        print("Resquest method: \(String(describing: request.httpMethod))\n\n")
        print("Request headers: \(String(describing: request.allHTTPHeaderFields))\n\n")
        print("Request parameters: \(String(describing: String(data: request.httpBody ?? Data(), encoding: .utf8)))\n\n")
        print("Response value: \(String(describing: String(describing: json)))\n\n")
        #endif
    }
}
