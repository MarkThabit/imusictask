//
//  ServerAPI.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

enum ServerAPI: Request
{
    case getAccessToken
    case searchSongs(query: String)
}

extension ServerAPI {
    var path: String {
        switch self
        {
        case .getAccessToken:
            return "v0/api/gateway/token/client"
            
        case .searchSongs:
            return "v2/api/sayt/flat"
        }
    }
    
    var method: HTTPMethod {
        switch self
        {
        case .getAccessToken:
            return .post
            
        case .searchSongs:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        var parameters = [String: Any]()
        
        switch self
        {
        case .getAccessToken:
            break
            
        case .searchSongs(let query):
            parameters["query"] = query
            parameters["limit"] = 20
            parameters["includeArtists"] = false
        }
        
        return parameters
    }
    
    var headers: [String : String]? {
        var headers = [String: String]()
        
        switch self {
        case .getAccessToken:
            let gatewayKey = Bundle.main.object(forInfoDictionaryKey: Constants.AppURLKeys.kGatewayKey) as? String ?? ""
            headers["X-MM-GATEWAY-KEY"] = gatewayKey
            
        case .searchSongs:
            let accessToken = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.kAccessToken) ?? ""
            headers["Authorization"] = "Bearer \(accessToken)"
        }
        
        return headers
    }
}
