//
//  FailureHandlerDelegate.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

protocol FailureHandlerDelegate: class {
    func handleError(_ error: Error)
}
