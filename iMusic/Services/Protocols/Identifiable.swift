//
//  Identifiable.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

protocol Identifiable {
    static var reusableIdentifier: String { get }
}

extension Identifiable {
    static var reusableIdentifier: String {
        return String(describing: self)
    }
}
