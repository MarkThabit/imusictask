//
//  Constants.swift
//  iMusic
//
//  Created by Mark Maged on 7/5/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

struct Constants {
    
    struct AppURLKeys {
        static let kBaseURL    = "BaseURL"
        static let kGatewayKey = "GatewayKey"
    }
    
    struct UserDefaultsKeys {
        static let kAccessToken = "ACCESS_TOKEN"
    }
}
