//
//  Logger.swift
//  Peak
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 EasyDevs. All rights reserved.
//

import Foundation

final class Logger {
    
    // MARK: - Class functions
    
    class func log(_ message: Any...) {
        #if DEBUG
        let lineSeperator = "***************************************"
        print("\n\n\(lineSeperator)\n\n\(message[0])\n\n\(lineSeperator)\n\n")
        #endif
    }
    
    class func logError(_ message: Any...) {
        #if DEBUG
        print("PeakError: \(message[0])")
        #endif
    }
}
