//
//  MusicError.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import Foundation

enum MusicError: Error {
    case noInternetConnection
    case invalidResponse
    case unknownError(messgae: String)
}
