//
//  SongCell.swift
//  iMusic
//
//  Created by Mark Maged on 7/6/19.
//  Copyright © 2019 Mark Maged. All rights reserved.
//

import UIKit

class SongCell: UITableViewCell, Identifiable {
    
    // MARK: - iVars
    
    var song: MusicItem! {
        didSet {
            configureCell()
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var musicImageView: RoundedImageView!
    @IBOutlet private weak var titleLbl: UILabel!
    @IBOutlet private weak var songTypeLbl: UILabel!
    @IBOutlet private weak var artistNameLbl: UILabel!
    
    // MARK: - Helper Methods
    
    private func configureCell() {
        selectionStyle = .none
        
        if let defaultImageSize = self.song.cover?.default {
            musicImageView.loadImage(urlString: defaultImageSize)
        }
        
        titleLbl.text = song.title
        songTypeLbl.text = song.type
        artistNameLbl.text = song.artist?.name
    }
}
